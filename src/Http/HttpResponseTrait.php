<?php

namespace Dsidorov\TaskTracker\Http;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;

trait HttpResponseTrait
{
    /**
     * @var ResponseFactoryInterface
     */
    protected $responseFactory;

    protected function success(string $data): ResponseInterface
    {
        $response = $this->responseFactory->createResponse(200);
        $response->getBody()->write($data);
        return $response;
    }

    protected function notFound(): ResponseInterface
    {
        return $this->responseFactory->createResponse(404);
    }

    protected function conflict(string $conflictMsg): ResponseInterface
    {
        $response = $this->responseFactory->createResponse(409);
        $response->getBody()->write($conflictMsg);

        return $response;
    }

    protected function redirect(string $location): ResponseInterface
    {
        $response = $this->responseFactory
            ->createResponse(302)
            ->withHeader('Location', $location)
        ;

        return $response;
    }

    protected function authorizationRequired(): ResponseInterface
    {
        $response = $this->responseFactory->createResponse(401);
        $response = $response->withHeader('WWW-Authenticate', 'Basic realm="Access to site"');

        return $response;
    }
}