<?php

namespace Dsidorov\TaskTracker\Http;


use Psr\Http\Message\ServerRequestInterface;

class RequestUriPathSpliter
{
    public static function split(string $toSplit, ServerRequestInterface $request): ServerRequestInterface
    {
        $oldPath = $request->getUri()->getPath();
        $newPath = explode($toSplit, $oldPath)[1];

        $newUri = $request->getUri()->withPath($newPath);
        return $request->withUri($newUri);
    }
}