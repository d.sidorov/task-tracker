<?php

namespace Dsidorov\TaskTracker\AdminTask;

use Dsidorov\TaskTracker\Task\DescriptionIsEmptyException;
use Dsidorov\TaskTracker\Task\Task;

class AdminestratedTask extends Task
{
    /**
     * @param string $newDescription
     * @throws DescriptionIsEmptyException
     */
    public function editDescription(string $newDescription): void
    {
        if (empty($newDescription)) {
            throw new DescriptionIsEmptyException();
        }

        $this->description = $newDescription;
    }

    public function done(): void
    {
        $this->isDone = true;
    }
}