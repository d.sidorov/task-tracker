<?php

namespace Dsidorov\TaskTracker\AdminTask\Controller;

use Dsidorov\TaskTracker\AdminTask\Service\AdminTaskService;
use Dsidorov\TaskTracker\Http\HttpResponseTrait;
use Dsidorov\TaskTracker\Http\RequestUriPathSpliter;
use Dsidorov\TaskTracker\Task\DescriptionIsEmptyException;
use Dsidorov\TaskTracker\Task\Service\TaskNotFoundException;
use Dsidorov\TaskTracker\TemplateEngine\TemplateEngineInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AdminTaskController implements RequestHandlerInterface
{
    use HttpResponseTrait;

    /**
     * @var AdminTaskService
     */
    private $adminTaskService;
    /**
     * @var TemplateEngineInterface
     */
    private $templateEngine;

    public function __construct(
        ResponseFactoryInterface $responseFactory,
        AdminTaskService $adminTaskService,
        TemplateEngineInterface $templateEngine
    ) {
        $this->responseFactory = $responseFactory;
        $this->adminTaskService = $adminTaskService;
        $this->templateEngine = $templateEngine;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (!$this->isAdmin($request)) {
            return $this->authorizationRequired();
        }

        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        if ($path === '') {
            return $this->taskList($request);
        }

        $path = explode('/', $path)[1];

        if ($path === 'done') {
            $request = RequestUriPathSpliter::split('/done', $request);
            return $this->doneTask($request);
        }

        if ($path === 'edit' && $method === 'GET') {
            $request = RequestUriPathSpliter::split('/edit', $request);
            return $this->showEdit($request);
        }

        if ($path === 'edit' && $method === 'POST') {
            $request = RequestUriPathSpliter::split('/edit', $request);
            return $this->edit($request);
        }

        return $this->notFound();
    }

    private function taskList(ServerRequestInterface $request): ResponseInterface
    {
        $page = $request->getQueryParams()['page'] ?? 1;
        if (!((int) $page)) {
            return $this->conflict('page must be int');
        }

        $sortBy = $request->getQueryParams()['sortBy'];

        try {
            $tasksDTOs = $this->adminTaskService->getTasksList($page - 1, $sortBy);
        } catch (\Exception $e) {
            return $this->conflict($e->getMessage());
        }

        $pagesCount = $this->adminTaskService->getPagesCount();
        $html = $this->templateEngine->render(
            'task-list.html.twig',
            [
                'isAdmin' => true,
                'tasks' => $tasksDTOs,
                'pages' => $pagesCount,
                'currentPage' => $page,
                'sortBy' => $sortBy
            ]
        );

        return $this->success($html);
    }

    private function doneTask(ServerRequestInterface $request): ResponseInterface
    {
        $taskId = $this->getTaskId($request);
        if (!isset($taskId)) {
            return $this->notFound();
        }

        try {
            $this->adminTaskService->doneTask($taskId);
        } catch (TaskNotFoundException $e) {
            return $this->notFound();
        }

        return $this->redirect('/admin');
    }

    private function showEdit(ServerRequestInterface $request): ResponseInterface
    {
        $taskId = $this->getTaskId($request);
        if (!isset($taskId)) {
            return $this->notFound();
        }

        try {
            $taskDto = $this->adminTaskService->getTask($taskId);
        } catch (TaskNotFoundException $e) {
            return $this->notFound();
        }

        $html = $this->templateEngine->render('admin-edit-task.html.twig', ['task' => $taskDto]);
        return $this->success($html);
    }

    private function edit(ServerRequestInterface $request): ResponseInterface
    {
        $taskId = $this->getTaskId($request);
        if (!isset($taskId)) {
            return $this->notFound();
        }

        $newDescription = $request->getParsedBody()['description'] ?? '';
        $newDescription = htmlspecialchars($newDescription);

        try {
            $this->adminTaskService->editTaskDescription($taskId, $newDescription);
        } catch (TaskNotFoundException $e) {
            return $this->notFound();
        } catch (DescriptionIsEmptyException $e) {
            return $this->conflict($e->getMessage());
        }

        return $this->redirect('/admin');
    }

    private function isAdmin(ServerRequestInterface $request): bool
    {
        $auth = $request->getHeader('Authorization');
        if (empty($auth)) {
            return false;
        }

        $withoutBearer = explode('Basic ', $auth[0]);
        $credentials = base64_decode($withoutBearer[1]);

        list($login, $password) = explode(':', $credentials);

        // it should be configured params. Now, it`s fine to be hardcoded
        return $login === 'admin' && $password === '123';
    }

    private function getTaskId(ServerRequestInterface $request): string
    {
        $path = $request->getUri()->getPath();
        $taskId = explode('/', $path)[1];
        return $taskId;
    }
}