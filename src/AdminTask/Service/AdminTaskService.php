<?php

namespace Dsidorov\TaskTracker\AdminTask\Service;


use Dsidorov\TaskTracker\AdminTask\AdminestratedTask;
use Dsidorov\TaskTracker\Task\DescriptionIsEmptyException;
use Dsidorov\TaskTracker\Task\Service\TaskDTO;
use Dsidorov\TaskTracker\Task\Service\TaskNotFoundException;
use Dsidorov\TaskTracker\Task\Service\TaskService;

class AdminTaskService extends TaskService
{
    /**
     * @param string $taskId
     * @param string $newDescription
     * @throws TaskNotFoundException
     * @throws DescriptionIsEmptyException
     */
    public function editTaskDescription(string $taskId, string $newDescription): void
    {
        if (!isset($this->tasks[$taskId])) {
            throw new TaskNotFoundException($taskId);
        }

        $task = $this->tasks[$taskId];
        $administratedTask = new AdminestratedTask(
            $task->getId(),
            $task->getUserName(),
            $task->getEmail(),
            $task->getDescription(),
            $task->isDone()
        );

        $administratedTask->editDescription($newDescription);

        unset($this->tasks[$taskId]);
        // add to array start cause all updated tasks should be first items
        $this->tasks = [$taskId => $administratedTask] + $this->tasks;
    }

    /**
     * @param string $taskId
     * @throws TaskNotFoundException
     */
    public function doneTask(string $taskId): void
    {
        if (!isset($this->tasks[$taskId])) {
            throw new TaskNotFoundException($taskId);
        }

        $task = $this->tasks[$taskId];
        $administratedTask = new AdminestratedTask(
            $task->getId(),
            $task->getUserName(),
            $task->getEmail(),
            $task->getDescription(),
            $task->isDone()
        );

        $administratedTask->done();

        unset($this->tasks[$taskId]);
        $this->tasks = [$taskId => $administratedTask] + $this->tasks;
    }

    /**
     * @param string $taskId
     * @return TaskDTO
     * @throws TaskNotFoundException
     */
    public function getTask(string $taskId): TaskDTO
    {
        if (!isset($this->tasks[$taskId])) {
            throw new TaskNotFoundException($taskId);
        }

        $task = $this->tasks[$taskId];
        $taskDtos = $this->tasksToDtos([$task]);
        return $taskDtos[0];
    }
}