<?php

namespace Dsidorov\TaskTracker\Task;


use Throwable;

class DescriptionIsEmptyException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct("Description can't be empty", $code, $previous);
    }

}