<?php

namespace Dsidorov\TaskTracker\Task\Service;


class TaskDTO
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $userName;

    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $description;
    /**
     * @var bool
     */
    public $isDone;

    public function __construct(
        string $id,
        string $userName,
        string $email,
        string $description,
        bool $isDone
    ) {
        $this->id = $id;
        $this->userName = $userName;
        $this->email = $email;
        $this->description = $description;
        $this->isDone = $isDone;
    }
}