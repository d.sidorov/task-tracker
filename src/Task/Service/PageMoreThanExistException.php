<?php

namespace Dsidorov\TaskTracker\Task\Service;


use Throwable;

class PageMoreThanExistException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct('"page" param more than pages exists', $code, $previous);
    }

}