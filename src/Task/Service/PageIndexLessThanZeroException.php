<?php

namespace Dsidorov\TaskTracker\Task\Service;


use Throwable;

class PageIndexLessThanZeroException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct('"page" param less than 0', $code, $previous);
    }
}