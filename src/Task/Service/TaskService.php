<?php

namespace Dsidorov\TaskTracker\Task\Service;


use Dsidorov\TaskTracker\Task\DescriptionIsEmptyException;
use Dsidorov\TaskTracker\Task\Task;

class TaskService
{
    const TASKS_PER_PAGE = 3;

    /**
     * @var Task[]
     */
    protected $tasks;
    private $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @param string $userName
     * @param string $email
     * @param string $description
     * @throws DescriptionIsEmptyException
     */
    public function createTask(string $userName, string $email, string $description): void
    {
        $id = uniqid();
        $newTask = new Task($id, $userName, $email, $description);
        $this->tasks = [$id => $newTask] + $this->tasks;
    }

    /**
     * @param int $pageIndex
     * @return TaskDTO[]
     * @throws PageIndexLessThanZeroException
     * @throws PageMoreThanExistException
     */
    public function getTasksList(int $pageIndex = 0, string $sortBy = null): array
    {
        if (empty($this->tasks)) {
            return [];
        }

        if ($sortBy === 'userName') {
            return $this->getTasksListSortedByUserName($pageIndex);
        } elseif ($sortBy === 'email') {
            return $this->getTasksListSortedByEmail($pageIndex);
        } elseif ($sortBy === 'done') {
            return $this->getTasksListSortedByDone($pageIndex);
        }

        $tasks = $this->getPage($pageIndex, $this->tasks);
        return $this->tasksToDtos($tasks);
    }

    /**
     * @param int $pageIndex
     * @param Task[] $tasks
     * @return Task[]
     * @throws PageIndexLessThanZeroException
     * @throws PageMoreThanExistException
     */
    protected function getPage(int $pageIndex, array $tasks): array
    {
        if ($pageIndex < 0) {
            throw new PageIndexLessThanZeroException();
        }

        $pages = $this->getPages($tasks);
        if ($pageIndex > count($pages) - 1) {
            throw new PageMoreThanExistException();
        }

        return $pages[$pageIndex];
    }

    /**
     * @param Task[] $tasks
     * @return array
     */
    protected function getPages(array $tasks): array
    {
        return array_chunk($tasks, self::TASKS_PER_PAGE);
    }

    /**
     * @param int $pageIndex
     * @return TaskDTO[]
     * @throws PageIndexLessThanZeroException
     * @throws PageMoreThanExistException
     */
    public function getTasksListSortedByUserName(int $pageIndex = 0): array
    {
        $sortFunc = function (Task $a, Task $b) {
            return $a->getUserName() > $b->getUserName();
        };

        $sortedTasks = $this->sortTasksBy($sortFunc, $this->tasks);
        $page = $this->getPage($pageIndex, $sortedTasks);
        return $this->tasksToDtos($page);
    }

    /**
     * @param int $pageIndex
     * @return array
     * @throws PageIndexLessThanZeroException
     * @throws PageMoreThanExistException
     */
    public function getTasksListSortedByEmail(int $pageIndex = 0): array
    {
        $sortFunc = function (Task $a, Task $b) {
            return $a->getEmail() > $b->getEmail();
        };

        $sortedTasks = $this->sortTasksBy($sortFunc, $this->tasks);
        $page = $this->getPage($pageIndex, $sortedTasks);
        return $this->tasksToDtos($page);
    }

    /**
     * @param int $pageIndex
     * @return array
     * @throws PageIndexLessThanZeroException
     * @throws PageMoreThanExistException
     */
    public function getTasksListSortedByDone(int $pageIndex = 0)
    {
        $sortFunc = function (Task $a, Task $b) {
            return $b->isDone();
        };

        $sortedTasks = $this->sortTasksBy($sortFunc, $this->tasks);
        $page = $this->getPage($pageIndex, $sortedTasks);
        return $this->tasksToDtos($page);
    }

    public function getPagesCount(): int
    {
        return count($this->getPages($this->tasks));
    }

    public function load(): void
    {
        if (isset($this->tasks)) {
            return;
        }

        if (!file_exists($this->fileName)) {
            $this->tasks = [];
            return;
        }

        $serialised = file_get_contents($this->fileName);
        if (empty($serialised)) {
            $this->tasks = [];
            return;
        }

        if (!($unserialised = unserialize($serialised))) {
            throw new \Exception('Cant unserialize file: ' . $this->fileName);
        }

        $this->tasks = $unserialised;
    }

    public function save(): void
    {
        if (!file_exists($this->fileName)) {
            file_put_contents($this->fileName, '', LOCK_EX);
        }

        if (!empty($this->tasks)) {
            $serialised = serialize($this->tasks);
            file_put_contents($this->fileName, $serialised, LOCK_EX);
        }
    }

    protected function tasksToDtos(array $tasks): array
    {
        return array_map(function (Task $task): TaskDTO {
            return new TaskDTO(
                $task->getId(),
                $task->getUserName(),
                $task->getEmail(),
                $task->getDescription(),
                $task->isDone()
            );
        }, $tasks);
    }

    private function sortTasksBy(callable $sortFunc, array $tasks): array
    {
        usort($tasks, $sortFunc);
        return $tasks;
    }
}