<?php

namespace Dsidorov\TaskTracker\Task\Service;


use Throwable;

class TaskNotFoundException extends \Exception
{
    public function __construct(string $taskId, int $code = 0, Throwable $previous = null)
    {
        parent::__construct("Task not found: ${taskId}", $code, $previous);
    }

}