<?php

namespace Dsidorov\TaskTracker\Task;

class Task
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $userName;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $description;

    protected $isDone = false;

    /**
     * @param string $id
     * @param string $userName
     * @param string $email
     * @param string $description
     * @param bool $isDone
     * @throws DescriptionIsEmptyException
     */
    public function __construct(string $id, string $userName, string $email, string $description, bool $isDone = false)
    {
        if (empty($description)) {
            throw new DescriptionIsEmptyException();
        }

        $this->id = $id;
        $this->userName = $userName;
        $this->email = $email;
        $this->description = $description;
        $this->isDone = $isDone;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function isDone(): bool
    {
        return $this->isDone;
    }
}