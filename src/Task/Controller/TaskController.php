<?php

namespace Dsidorov\TaskTracker\Task\Controller;

use Dsidorov\TaskTracker\Http\HttpResponseTrait;
use Dsidorov\TaskTracker\Task\DescriptionIsEmptyException;
use Dsidorov\TaskTracker\Task\Service\TaskService;
use Dsidorov\TaskTracker\TemplateEngine\TemplateEngineInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class TaskController implements RequestHandlerInterface
{
    use HttpResponseTrait;

    /**
     * @var TaskService
     */
    private $taskService;
    /**
     * @var TemplateEngineInterface
     */
    private $templateEngine;

    public function __construct(
        ResponseFactoryInterface $responseFactory,
        TaskService $taskService,
        TemplateEngineInterface $templateEngine
    ) {
        $this->responseFactory = $responseFactory;
        $this->taskService = $taskService;
        $this->templateEngine = $templateEngine;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        if ($path === '/' && $method === 'GET') {
            return $this->getTaskList($request);
        }

        if ($path === '/new' && $method === 'GET') {
            return $this->showCreateNewTask();
        }

        if ($path === '/new' && $method === 'POST') {
            return $this->createTask($request);
        }

        return $this->notFound();
    }

    private function getTaskList(ServerRequestInterface $request): ResponseInterface
    {
        $page = $request->getQueryParams()['page'] ?? 1;
        if (!((int) $page)) {
            return $this->conflict('page must be int');
        }

        $sortBy = $request->getQueryParams()['sortBy'];

        try {
            $tasksDTOs = $this->taskService->getTasksList($page - 1, $sortBy);
        } catch (\Exception $e) {
            return $this->conflict($e->getMessage());
        }

        $pagesCount = $this->taskService->getPagesCount();
        $html = $this->templateEngine->render(
            'task-list.html.twig',
            [
                'tasks' => $tasksDTOs,
                'pages' => $pagesCount,
                'currentPage' => $page,
                'sortBy' => $sortBy
            ]
        );

        return $this->success($html);
    }

    private function showCreateNewTask(): ResponseInterface
    {
        $html = $this->templateEngine->render('new-task.html.twig');
        return $this->success($html);
    }

    private function createTask(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();
        $userName = $body['user-name'] ?? '-no user-';
        $email = $body['email'] ?? '-no email-';
        $description = $body['description'] ?? '';

        $userName = htmlspecialchars($userName);
        $email = htmlspecialchars($email);
        $description = htmlspecialchars($description);

        try {
            $this->taskService->createTask($userName, $email, $description);
        } catch (DescriptionIsEmptyException $e) {
            return $this->conflict($e->getMessage());
        }

        return $this->redirect('/');
    }
}