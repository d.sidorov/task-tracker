<?php

namespace Dsidorov\TaskTracker\TemplateEngine;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigTemplateEngine implements TemplateEngineInterface
{
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(string $templateDirPath)
    {
        $loader = new FilesystemLoader($templateDirPath);
        $this->twig = new Environment($loader);
    }

    public function render(string $template, array $params = []): string
    {
        return $this->twig->render($template, $params);
    }
}