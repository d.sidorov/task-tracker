<?php

namespace Dsidorov\TaskTracker\TemplateEngine;


interface TemplateEngineInterface
{
    public function render(string $template, array $params = []): string;
}