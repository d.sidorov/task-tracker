FROM php:7.2-cli-alpine as dev
EXPOSE 80
WORKDIR /app
COPY . .
CMD ["php", "-S0.0.0.0:80", "-tpublic"]

FROM composer as build-prod
WORKDIR /app
COPY composer.json .
COPY composer.lock .
RUN composer install -o --no-dev --ignore-platform-reqs

FROM php:7.2-cli-alpine as prod
EXPOSE 80
WORKDIR /app
COPY --from=build-prod /app/vendor vendor
COPY . .
CMD ["php", "-S0.0.0.0:80", "-tpublic"]
