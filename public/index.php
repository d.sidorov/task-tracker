<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Dsidorov\TaskTracker\AdminTask\Controller\AdminTaskController;
use Dsidorov\TaskTracker\AdminTask\Service\AdminTaskService;
use Dsidorov\TaskTracker\Http\RequestUriPathSpliter;
use Dsidorov\TaskTracker\Task\Controller\TaskController;
use Dsidorov\TaskTracker\Task\Service\TaskService;
use Dsidorov\TaskTracker\TemplateEngine\TwigTemplateEngine;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;

ob_start();

$taskRepoFile = __DIR__ . '/../tasks.repo';

$request = ServerRequestFactory::fromGlobals();
$responseFactory = new ResponseFactory();

$templateEngine = new TwigTemplateEngine(__DIR__ . '/../view');

// simple routing
$path = $request->getUri()->getPath();
$isAdmin = explode('/', $path)[1] === 'admin';
if ($isAdmin) {
    $taskService = new AdminTaskService($taskRepoFile);
    $taskService->load();

    $handler = new AdminTaskController($responseFactory, $taskService, $templateEngine);
    $request = RequestUriPathSpliter::split('/admin', $request);
} else {
    $taskService = new TaskService($taskRepoFile);
    $taskService->load();

    $handler = new TaskController($responseFactory, $taskService, $templateEngine);
}

try {
    $response = $handler->handle($request);
} catch (Exception $e) {
    $response = $responseFactory->createResponse(500);
    $response->getBody()->write($e->getMessage());
}

header($_SERVER['SERVER_PROTOCOL'] . ' ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase());
foreach ($response->getHeaders() as $name => $values) {
    header($name . ": " . implode(", ", $values));
}

echo $response->getBody();

$taskService->save();

ob_end_flush();
